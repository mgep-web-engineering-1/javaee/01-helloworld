<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Hello World</title>
	</head>
	<body>
		<!-- This is HTML code -->
		<p>This is a HTML text</p>
		<p><%= new String("This is a Java String.") /* This is Java Code */ %></p>
	</body>
</html>