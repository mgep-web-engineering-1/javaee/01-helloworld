# HelloWorld JSP

## Make it work

* Clone the repository:

```console
$ git clone https://gitlab.com/mgep-web-engineering-1/javaee/01-helloworld.git
Cloning into '01-helloworld'...
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 4 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (4/4), done.
```

* Open ```01-helloworld``` folder with VSCode.
* Right click and ```Debug Java``` on file ```src > main > java > ... > Main.java```.
* Open http://localhost:8080 in the browser.

## Explainations

We will explain 2 files:

1. Main.java
1. index.jsp

### 1.- Main.java

This will launch a Tomcat instance with the current web application. It will make 4 main things:

1. Configure the PORT where tomcat will serve the web application.
1. Stablish where the "non-java" files will be placed (html, css, js, jsp...).
1. Configure WEB-INF folder.
1. Launch the Tomcat server.

#### Port configuration

To set the port we could do:

```java
// Main.java
...
tomcat.setPort(8080);
...
```

It would be simpler but would not adapt for real deploys. The following code would load the port from an environment variable called "PORT". If the environment variable does not exist, it will use the default port (8080):

```java
// Main.java
...
String webPort = System.getenv("PORT");
if(webPort == null || webPort.isEmpty()) {
    webPort = "8080";
}

tomcat.setPort(Integer.valueOf(webPort));
...
```

#### Webapp

We will define where the webapp folder is. It will contain all the "non-java" files: JSP, HTML, CSS, images...

```java
// Main.java
...
String webappDirLocation = "src/main/webapp/";
...
StandardContext ctx = (StandardContext) tomcat.addWebapp("/", new File(webappDirLocation).getAbsolutePath());
...
```

#### WEB-INF

WEB-INF folder contains files accessible for the back-end (Servlets) but not for the final user (Browsers).

```java
//Main.java
...
// Declare an alternative location for your "WEB-INF/classes" dir
// Servlet 3.0 annotation will work
File additionWebInfClasses = new File("target/classes");
WebResourceRoot resources = new StandardRoot(ctx);
resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",
        additionWebInfClasses.getAbsolutePath(), "/"));
ctx.setResources(resources);

tomcat.enableNaming();
tomcat.getConnector();
...
```

#### Tomcat Server

We will create the server and launch it.

```java
// Main.java
...
Tomcat tomcat = new Tomcat();
...
tomcat.start();
...
tomcat.getServer().await();
...
```

### 2.- index.jsp

As we have set ```webapp``` folder as the main *non-java* file folder, Tomcat will search for an ```index.html``` or ```index.jsp``` file in that folder.

If a ```index.html``` file was found, it would be sent directly as a response.

As no ```index.html``` will be found, the server will load ```index.jsp```, execute it and generate the response.

Looking to the code of the JSP file, we can see that is very similar to an HTML file. The reason of this is that we want an HTML as a response to ``` GET http://localhost:8080/ ```.

We can see 2 things that would never appear in an HTML:

* (1) The first line before the HTML doctype will tell the server that the file is a JSP.

```jsp
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
...
```

* (2) The JAVA code inside the HTML code between ```<%=``` and ```/>``` (or ```<%``` and ```/>``` that is not in this example), that will be executed.

```jsp
<%= new String("This is a Java String.") /* This is Java Code */ %>
```

JSP files will be executed in the **server side**, but **never** sent as a response. Browser do not know how to execute Java/JSP code, so Tomcat executes JSP and the Java code inside and generates the needed HTML. Then, that HTML is sent to the browser as the response to its request.

The response that Tomcat will send after executing the JSP file (ant therefore the code that the browser will execute) will be the following:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Hello World</title>
  </head>
  <body>
    <!-- This is HTML code -->
    <p>This is a HTML text</p>
    <p>This is a Java String.</p>
  </body>
</html>
```

This is clearly seen with the comments. If you _view page source_ in the web browser, you will see the HTML comments but no the Java comments. The browser does **NEVER** execute Java code! Java comment was not even sent in the response.

**WARNING! Remember that JavaScript != Java.**

## Next and before

* Before [00-maven-embedded-tomcat](https://gitlab.com/mgep-web-engineering-1/javaee/00-maven-embedded-tomcat)
* Next [02-retrieve-parameters](https://gitlab.com/mgep-web-engineering-1/javaee/02-retrieve-parameters)